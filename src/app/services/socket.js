import expressSession from 'express-session';
import connectRedis from 'connect-redis';
import redis from 'redis';
import { io } from 'server';
import { SESSION, DB } from 'config';

const release = (io) => {
    const releaseNamespace = io.of('/release');
    releaseNamespace.use((socket, next) => {
        if (socket.request.sessionID) return next();
        next(new Error('Authentication error'));
    });
    releaseNamespace.on('connection', socket => {
        socket.emit('a message', {
            that: 'only'
            , '/release': 'will get'
        });
        socket.on('task-finish', (from, data) => {
            console.log(from, socket.id);
        });
        releaseNamespace.emit('a message', {
            everyone: 'in'
            , '/release': 'will get'
        });
        socket.on('disconnect', function () {
            io.emit('user disconnected');
        });
    });
};

const init = () => {
    const RedisStore = connectRedis(expressSession);
    const session = expressSession({ ...SESSION, store: new RedisStore({ client: redis.createClient(DB.redis.url, {...DB.redis}) }) });
    io.use((socket, next) => session(socket.request, socket.request.res, next));
    release(io);
};

export { init };