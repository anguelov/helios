import express from 'express';
import privy from 'services/privy';

const router = express.Router();

router.use('/public', express.static('public'), (req, res) => res.end());

router.use(privy);

export default router;

