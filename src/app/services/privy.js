import expressSession from 'express-session';
import connectRedis from 'connect-redis';
import redis from 'redis';
import express from 'express';
import { SESSION, DB } from 'config';
import MySQL from 'modules/mysql/MySQL';

const router = express.Router();
const RedisStore = connectRedis(expressSession);

router.use(expressSession({ ...SESSION, store: new RedisStore({ client: redis.createClient(DB.redis.url, {...DB.redis}) }) }));

router.get('/release', (req, res, next) => {
    res.render('index');
});

router.get('/mysql', async (req, res, next) => {
    try {
        const conn = new MySQL();
        const result = await conn.query('SELECT * FROM project LEFT JOIN deploy ON deploy.id = project.idDeploy;');
        res.json(result);
    } catch (Exception) {
        next(Exception);
    }
});

export default router;
