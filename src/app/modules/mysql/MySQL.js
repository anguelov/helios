import { promisify } from 'util';
import mysql from 'mysql';
import { DB } from 'config';

const poll = mysql.createPool({ ...DB.mysql });

class MySQL {
    constructor(connectionPool = poll) {
        this.connectionPool = connectionPool;
        this.getConnection = promisify(this.connectionPool.getConnection).bind(this.connectionPool);
    }

    async query(sql) {
        try {
            const connection = await this.getConnection();
            return await promisify(connection.query).bind(connection)(sql);
        } catch (Exception) {
            throw Exception;
        }
    }

}

export default MySQL;