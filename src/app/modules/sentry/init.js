import raven from 'raven';
import { SENTRY } from 'config';

export const init = async () => {
    try {
        await raven.config(SENTRY.url, SENTRY.config).install();
    } catch (Exception) {
        throw Exception;
    }
};
