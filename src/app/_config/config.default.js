import OS from 'os';
import path from 'path';

export const app = {
    port: 3030,
    dns: 'localhost',
    bind: '0.0.0.0',
    ip: '127.0.0.1',
    hostname: OS.hostname(),
    base: path.join(__dirname, '..', '..'),
    env: process.env.config_env || process.env.NODE_ENV || 'LOCAL',
    _debug: false,
    envs: {
        LOCAL: 'LOCAL',
        DEVELOPMENT: 'DEVELOPMENT',
        STAGING: 'STAGING',
        PRODUCTION: 'PRODUCTION'
    },
    get debug() {
        return this._debug;
    },
    set debug(mode) {
        if (typeof mode === 'boolean' && (mode !== this._debug)) {
            this._debug = mode;
            //event.emit('config.debug.change', this._debug);
        }
    },
    get publicURL() {
        return `http://${this.dns}:${this.port}`;
    },
};

export const DB = {
    mongo: {
        protocol: 'mongodb',
        host: 'localhost',
        port: '27017',
        schema: 'helios',
        connectionRetryTimeOut: 3000,
        get url() {
            return `${DB.mongo.protocol}://${DB.mongo.host}:${DB.mongo.port}/${DB.mongo.schema}`;
        }
    },
    mysql: {
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        port: 3306,
        localAddress: '127.0.0.1',
        database: 'helios',
        connectionLimit: 10
    },
    mongoose: {
        autoIndex: true
    },
    redis: {
        host: '127.0.0.1',
        port: '6379',
        db: 0,
        retry_strategy: opt => {
            if (opt.error && opt.error.code === 'ECONNREFUSED' && opt.attempt < DB.redis.attempts) {
                return DB.redis.interval;
            } else {
                event.emit('redis.conn.fail', opt);
            }
        },
        get interval() {
            return 3000;
        },
        get attempts() {
            return 10;
        },
        get url() {
            return `redis://${DB.redis.host}:${DB.redis.port}`;
        }
    }
};

export const SENTRY = {
    project: '47',
    key: '505b254f06374f9ba7e8fd737b2852e8:b13f8a3a57c14600b70aba107905396a',
    public_key: '505b254f06374f9ba7e8fd737b2852e8',
    host: '10.151.52.125',
    get url() {
        return `http://${SENTRY.key}@${SENTRY.host}/${SENTRY.project}`;
    },
    config: {
        environment: 'config.env',
        logger: 'default',
        name: OS.hostname(),
        tags:{},
        extra: {},
        autoBreadcrumbs: {
            'console': true,
            'http': true,
        },
        maxBreadcrumbs: 100,
        SENTRY_RELEASE: undefined,
        captureUnhandledRejections: true,
        sendTimeout: 5
    },
    captureMessageOptions: {
        level: 'info'
    }
};

export const SOCKETIO = {
    options: {
        path: '/helios-io',
        serveClient: true
    }
};

export const SESSION = {
    cookie: {
        path: '/',
        httpOnly: true,
        secure: false,
        maxAge: 15552000000, // 6 months
        SameSite: true,
    },
    secret: 'Hizd6$/KSMwofa01$-s2SkJ9FALwAE6',
    proxy: true,
    unset: 'keep',
    resave: false,
    saveUninitialized: true,
    rolling: true,
    name: 'ZAID',
};