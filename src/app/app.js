import express from 'express';
import { bootstrap } from 'app/bootstrap';
import router from 'services/router';

export const app = express();

export const initializeApplication = async () => {
    await bootstrap();
    app.set('view engine', 'ejs');
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    app.use(router);
};

