import { init as mongo } from 'modules/mongo/init';
import {init as socket } from 'services/socket';
export const bootstrap = async () => {
  try {
      await mongo();
      socket();
  } catch (Exception) {
      throw Exception;
  }
};