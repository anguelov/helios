import http from 'http';
import socket from 'socket.io';
import { app as config, SOCKETIO } from 'config';
import { app, initializeApplication } from 'app/app';

const server = http.Server(app);
const io = socket(server, SOCKETIO.options);

export {
    server,
    app,
    io
};

initializeApplication().then(() => {
    server.listen(config.port).on('listening', () => console.log(`Started @ ${config.port}`));
});
