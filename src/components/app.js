import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import io from 'socket.io-client';

class App extends Component {
    constructor(props) {
        super(props);
        this.id = Math.random().toString();
    }

    componentDidMount() {
        const socket = io('/release', {path: '/helios-io'});
        socket.emit('task-finish', { id: this.id });
        socket.on('disconnect', data => {
            console.log(data);
        });
    }

    render() {
        return <h1>Hello, from react...! ID: {this.id}</h1>
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);

